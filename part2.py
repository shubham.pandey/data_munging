from common import*
my_dog = Extract_Data('football.dat')
data = my_dog.extract()
min_difference = 10**20
for line in data:
    if len(line.split()) > 1:
        line = line.split()
        team = line[1]
        value = Base_For_Calculation(
            start_=6, end_=8, list_=line, resulting_column=team)
        diff = value.difference_calculator()
        # x = Minimun_difference(diff=diff, team=team)
        # print(x.calculate())
        if diff < min_difference:
            team_min = value.resulting_column
            min_difference = diff
print("Team --------- Goal_Difference")
print(f"{team_min}----- {min_difference}")
