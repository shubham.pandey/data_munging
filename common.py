class Extract_Data():
    def __init__(self, file_):
        self.file = file_

    def extract(self):
        with open(self.file) as temp:
            next(temp)
            for lines in temp:
                yield lines


class Base_For_Calculation():
    def __init__(self, start_, end_, list_, resulting_column):
        self.start = start_
        self.end = end_
        self.list = list_
        self.resulting_column = resulting_column

    def difference_calculator(self):
        max_t = float(self.list[self.start].strip('*'))
        min_t = float(self.list[self.end].strip('*'))
        print(max_t, min_t)
        difference = abs(max_t-min_t)
        return int(difference)


class Football(Base_For_Calculation):
    def __init__(self, start_, end_, list_, resulting_column):
        super().__init__(start_, end_, list_, resulting_column)

    # def difference_calculator(self):
    #     goal_for = self.list[self.start]
    #     goal_against = self.list[self.end]
    #     diff = abs(int(goal_against)-int(goal_for))
    #     return diff


class Minimun_difference():
    minimum = 10**20
    team = ""

    def __init__(self, diff, team):
        self.diff = diff
        self.team = team

    def calculate(self):
        if self.diff < self.minimum:
            self.minimum = self.diff
            self.team = self.team
        return self.minimum
