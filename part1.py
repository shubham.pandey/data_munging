from common import*

min_temp_diff = 10**20
weather_details = Extract_Data(file_='weather.dat')
data = weather_details.extract()
for lines in data:
    if len(lines) > 1:
        line = lines.split()
        day = line[0]
        value = Base_For_Calculation(
            start_=1, end_=2, list_=line, resulting_column=day)
        diff = value.difference_calculator()
        if diff < min_temp_diff:
            min_temp_diff = float(diff)
            day_number = value.resulting_column
print("Day------Spread")
print(f"{day_number} ------ {min_temp_diff}")
