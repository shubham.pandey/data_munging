DATA MUNGING PROJECT:

In this project we are given with two files named weather.dat and football.dat

Tasks Given:
1. Find the minimum temperature spread for the day and print result as day and its minimum temperature spread.
2. Find the minimum difference between goals for and goals against for a team and print the result as team and the minimum difference.

Approach Followed:
1. The goal was at first to implement the tasks given through spike implementation.
2. Do the same task by keeping in mind the SOLID principles.
3. part1.py is the file for the first task.
4. part2.py is the file for the second task.
5. common.py is the file that contains classes used bby both the files.